import DecisionController from './DecisionController'

describe('DecisionController', () => {
  describe('instance', () => {
    test('should contain the needed constants', () => {
      expect(new DecisionController()).toMatchObject({
        USE_PRICE_TOLERANCE_LIMIT_IN_ICEBERG: expect.any(Boolean),
        TRADE_ACTIONS: expect.any(Object),
        ACTIVE_CHAIN_STATUSES: expect.any(Array),
        TRANSACTIONS_SESSION_STATUS_RECHECK_PERIOD: expect.any(Number),
        MAX_INITIATOR_IDLE_TIME_AFTER_FIRST_OFFER: expect.any(Number),
        SESSIONS_CHAIN_STATUSES: expect.any(Object),
        FIRST_OFFER_SPREAD_MAX_DEVIATION: expect.any(Number),
        MAX_ACCEPTED_OFFER_PRICE_DEVIATION_PERC: expect.any(Number),
        BEST_OFFER_SEND_AFTER: expect.any(Number)
      })
    })
  })
})
