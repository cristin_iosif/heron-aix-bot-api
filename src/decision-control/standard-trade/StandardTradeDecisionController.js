import DecisionController from '../DecisionController'

class StandardTradeDecisionController extends DecisionController {
  constructor () {
    super()

    this.shouldFinalizeStandardTrade =
      this.shouldFinalizeStandardTrade.bind(this)
  }

  shouldFinalizeStandardTrade (params) {
    const {
      isIcebergTrade,
      chainStatus,
      entireChainTradeRequestAnswersNo,
      tradeVolumeLeft
    } = params

    return isIcebergTrade === false &&
      tradeVolumeLeft === 0 &&
      chainStatus !== this.SESSIONS_CHAIN_STATUSES.FINISHED &&
      entireChainTradeRequestAnswersNo > 0
  };
}

export default StandardTradeDecisionController
