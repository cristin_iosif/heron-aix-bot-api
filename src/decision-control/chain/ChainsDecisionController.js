import DecisionController from '../DecisionController'

class ChainsDecisionController extends DecisionController {
  constructor () {
    super()

    this.shouldMarkChainAsStalled = this.shouldMarkChainAsStalled.bind(this)
    this.shouldStopTransactionsChainByTimeout =
      this.shouldStopTransactionsChainByTimeout.bind(this)
    this.shouldCancelChain = this.shouldCancelChain.bind(this)
  }

  shouldMarkChainAsStalled (params) {
    const { tradeActionWasExpressed, timePassedSinceFirstOffer } = params

    if (
      typeof tradeActionWasExpressed !== 'boolean' ||
      typeof timePassedSinceFirstOffer !== 'number'
    ) {
      return false
    }

    return !tradeActionWasExpressed &&
      timePassedSinceFirstOffer > this.MAX_INITIATOR_IDLE_TIME_AFTER_FIRST_OFFER
  }

  shouldStopTransactionsChainByTimeout (params) {
    const {
      duration,
      maxDuration,
      chainStatus,
      numberOfOffersSentToInitiator
    } = params

    let remainingTime = maxDuration - duration
    let recheckPeriod = this.TRANSACTIONS_SESSION_STATUS_RECHECK_PERIOD

    return this.ACTIVE_CHAIN_STATUSES.includes(chainStatus) &&
      (
        remainingTime <= recheckPeriod ||
        (
          numberOfOffersSentToInitiator === 0 &&
          (duration >= this.BEST_OFFER_SEND_AFTER + 2 * recheckPeriod)
        )
      )
  };

  shouldCancelChain (params) {
    const { chainStatus } = params

    return chainStatus === this.SESSIONS_CHAIN_STATUSES.CANCELLED
  };
}

export default ChainsDecisionController
