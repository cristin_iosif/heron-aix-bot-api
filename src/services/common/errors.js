/**
 * The purpose of this file is to define all the errors thrown in /services.
 */

export const SessionNotDefinedError = new Error(
  'Session object cannot be undefine/null!'
)

export const SessionFieldOverrideError = fieldName => new Error(
  `Session field ${fieldName} already has a value assigned. Could not override.`
)

export const SessionFieldUnassignError = (fieldName, expectedType) => new Error(
  `Could not unassign session field ${fieldName}. Could not find value of ` +
  `type ${expectedType}.`
)

export const IterateeNotAFunctionError = new Error(
  'Iteratee is not of type function. Expected iteratee to be a function.'
)

export const InvalidBestSpreadFormatResultInputError = new Error(
  'Invalid input parameters for BestSpreadCalculator.formatResult() function.'
)

export const NoTradeVolumeAvailableError = new Error(
  'No volume left to trade with!'
)

export default {
  SessionNotDefinedError,
  SessionFieldOverrideError,
  SessionFieldUnassignError,
  IterateeNotAFunctionError,
  InvalidBestSpreadFormatResultInputError,
  NoTradeVolumeAvailableError
}
