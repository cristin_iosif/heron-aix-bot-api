/**
 * Helper class to handle work with objects.
 * @type {Class}
 */
class ObjectHelper {
  constructor () {
    this.flatten = this.flatten.bind(this)
  }

  /**
   * Flattenes the given object recursively so at the end it has max-depth = 1.
    Ex: obj = {
      a: { a1: { a11: 1 }, a2: 2, a3: null },
      b: 'test',
    }; will be flattened to:
    {
      'a.a1.a11': 1,
      'a.a2': 2,
      'a.a3': null,
      'b': 'test'
    }
   * @param  {Object} obj The object to flatten.
   * @return {Object}    Object of max-depth = 1.
   */
  flatten (obj) {
    let toReturn = {}

    for (let i in obj) {
      if (!obj.hasOwnProperty(i)) continue

      if ((typeof obj[i]) === 'object' && obj[i] != null) {
        let flatObject = this.flatten(obj[i])
        for (let x in flatObject) {
          if (!flatObject.hasOwnProperty(x)) continue

          toReturn[i + '.' + x] = flatObject[x]
        }
      } else {
        toReturn[i] = obj[i]
      }
    }

    return toReturn
  };
}

export default ObjectHelper
