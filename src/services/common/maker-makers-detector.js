/**
 * This script is specialised in finding the available market makers at the
 * beginning of a transactions session.
 */
import difference from 'lodash/difference'
import uniq from 'lodash/uniq'

import common from './common'
import { Trader } from '../../api/trader'
import { Transaction } from '../../api/transaction'
import { Session } from '../../api/session'
import { TransactionsSession } from '../../api/transactions-session'
import {
  ACTIVE_TRANSACTION_STATUSES,
  ACTIVE_TRANSACTIONS_SESSION_STATUSES,
  TRADER_TYPES,
  SEND_RATE_REQUEST_ONLY_TO_USERS_WITH_SESSION
} from '../../constants'

/**
 * Searches for available traders and returns the list of their traderIds. An
 * available trader is the trader which is not involved in any open
 * transactions.
 * @param {Object} Params object containing the following fields:
 * financialInstrumentId: Financial instrument for which to look
 * for available traders.
 * initiatorTraderId: The initiator trader id.
 * traderType: The trader type for which to look for.
 * @return {Array} Array with the available trader ids.
 */
export const findCurrentlyAvailable = ({
  financialInstrumentId,
  initiatorTraderId,
  traderType
}) => {
  /* First, we search for the interested traders. A trader is considered to be
  interested if he is not the initiator, he has the financial instrument with
  the requested id in use, and his traderType matches the requested traderType.
  */
  let interestedTradersQuery = {
    _id: { $not: { $eq: initiatorTraderId } },
    financialInstrumentsInUse: { $elemMatch: { $eq: financialInstrumentId } },
    traderType: traderType || { $in: TRADER_TYPES }
  }
  let interestedTraderIds = []
  let activeTransactionsTraderIds = []
  let initiatorTraderIds = []

  return Trader
    .find(interestedTradersQuery, { _id: 1 })
    .then(interestedTraderEntities => {
      interestedTraderIds = interestedTraderEntities.map(t => '' + t._id)

      /* Then we search for the interested traders which are involved into
      active transactions. (Find the transactions and keep the marketMakerId). */
      let tradersTransactionsQuery = {
        marketMakerId: { $in: interestedTraderIds },
        status: { $in: ACTIVE_TRANSACTION_STATUSES }
      }

      return Transaction.find(tradersTransactionsQuery, { marketMakerId: 1 })
    })
    .then(activeTradersTransactionEntities => {
      activeTransactionsTraderIds = activeTradersTransactionEntities
        .map(e => e.marketMakerId)

      /* Finally search for the interested traders which are initiators of
      transaction sessions. */
      let initiatorTradersTransactionsQuery = {
        initiatorTraderId: { $in: interestedTraderIds },
        overallStatus: { $in: ACTIVE_TRANSACTIONS_SESSION_STATUSES }
      }

      return TransactionsSession.find(
        initiatorTradersTransactionsQuery, { initiatorTraderId: 1 }
      )
    })
    .then(transactionsSessionEntites => {
      initiatorTraderIds = uniq(
        transactionsSessionEntites.map(ts => ts.initiatorTraderId)
      )

      /* Compute the truly available traders by removing the active transaction
      traders and the initiator traders from the set of interested traders */
      let availabledTraderIds = difference(
        interestedTraderIds,
        activeTransactionsTraderIds,
        initiatorTraderIds
      )

      console.log('@ 1) interestedTraderIds', interestedTraderIds)
      console.log('@ 2) activeTransactionsTraderIds', activeTransactionsTraderIds)
      console.log('@ 3) initiatorTraderIds', initiatorTraderIds)
      console.log('@ = availabledTraderIds', availabledTraderIds)

      return Promise.resolve(availabledTraderIds)
    })
    .then(availabledTraderIds => {
      const availableTraderObjectIds = common
        .stringsToObjectIds(availabledTraderIds)

      if (SEND_RATE_REQUEST_ONLY_TO_USERS_WITH_SESSION) { // app constant
        /* Search for the available traders which have a session in db and
        return their ids. */
        const tradersWithSessionQuery = {
          'session.trader._id': { $in: availableTraderObjectIds }
        }

        return Session
          .find(tradersWithSessionQuery, { 'session.trader._id': 1 })
          .then(entities => {
            let traderIds = entities.map(e => '' + e.session.trader._id)

            return Promise.resolve(uniq(traderIds))
          })
      }

      // Return the available trader ids.
      return Promise.resolve(availableTraderObjectIds)
    })
    .catch(err => {
      console.log('Failed to determine marketMakers list, ERR:', err)

      return Promise.reject(new Error('Failed to determine marketMakers list'))
    })
}

export default {
  findCurrentlyAvailable
}
