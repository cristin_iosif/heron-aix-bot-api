import ArrayHelper from './array-helper/ArrayHelper'
import ObjectHelper from './object-helper/ObjectHelper'

// export a singleton instance of the ArrayHelper class
export const arrayHelper = new ArrayHelper()
// export a singleton instance of the ObjectHelper class
export const objectHelper = new ObjectHelper()

export default {
  arrayHelper,
  objectHelper
}
