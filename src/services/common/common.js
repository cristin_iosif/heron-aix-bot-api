import { Types } from 'mongoose'

/**
 * Converts the given array of strings, to objectIds.
 * @param  {Array}  [ids=[]] Array of string.
 * @return {Array}          Array of objectIds produced from the input strings.
 */
export const stringsToObjectIds = (ids = []) => ids.map(Types.ObjectId)

export default {
  stringsToObjectIds
}
