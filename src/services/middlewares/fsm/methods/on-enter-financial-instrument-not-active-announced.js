import messages from '../../../../user-messages'

const onEnterFinancialInstrumentNotActiveAnnounced = ({ session }) => {
  session.watson = {}
  // reset watson detected workspace ids
  session.initiatorWorkspaceId = null
  session.receiverWorkspaceId = null
  // send informative message to the user
  session.outputMessage = [messages.common.financialInstrumentNotSupported]

  return Promise.resolve()
}

export default onEnterFinancialInstrumentNotActiveAnnounced
