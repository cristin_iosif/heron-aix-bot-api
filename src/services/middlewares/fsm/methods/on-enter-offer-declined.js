import events, { eventNames } from '../../../events-system/event-emitter'
import { TRANSACTION_STATUSES } from '../../../../constants'
import dashboardAPI from '../../../dashboard-api-wrapper'
import {
  TransactionsSessionManager,
  TransactionManager
} from '../../../../managers'
import messages from '../../../../user-messages'

/**
 * Defines the behaviour when a receiver declines a trade confirmation
 * question.
 */
const onEnterOfferDeclined = (
  { session },
  transition,
  isAutomaticallyDeclined // if declined without a receiver refusal message
) => {
  const transaction = session.activeTransaction

  if (!transaction) {
    console.log('No active transaction found.')
    return Promise.reject(new Error('No active transaction.'))
  }

  // reset watson context
  session.setContext = true
  session.watson = {}
  session.initiatorWorkspaceId = null
  session.receiverWorkspaceId = null
  session.transactionsSessionId = null
  session.transactionsSessionChainId = null

  return TransactionManager
    .findAllDependenciesOfTransactionWithId(transaction.getId())
    .then(dependencies => {
      dashboardAPI.notifyDashboardTransactions()

      let twoWayQuoteRequestRefused = transaction.status !==
        TRANSACTION_STATUSES.CONFIRMATION_ASKED

      if (twoWayQuoteRequestRefused && !isAutomaticallyDeclined) {
        return handleQuoteCancelledAfterSend(transaction)
      }
      transaction.setStatus(TRANSACTION_STATUSES.DECLINED)

      return transaction.save()
    })
    .then(() => {
      if (isAutomaticallyDeclined) {
        return handleQuoteAutomaticallyDeclined(transaction)
      }

      // inform the initiator that his bid/offer was declined
      if (transaction.getCloseAction()) {
        /* Only send it a close action is set on the transaction. This means
        that the receiver was asked to confirm the trade by the initiator. */
        events.emit(eventNames.SEND_MESSAGE_TO_TRADERS, {
          traderIds: [transaction.getInitiatorId()],
          message: messages.initiator.tradeIntentionDeclinedFn(
            transaction.getCloseAction()
          )
        })

        // keep this transaction live (use the trader's offer in other spreads)
        transaction.setStatus(TRANSACTION_STATUSES.RECEIVER_ANSWERED)

        return transaction.save()
      }
    })
}

const handleQuoteAutomaticallyDeclined = transaction => {
  events.emit(eventNames.PREPARE_TRADERS_FOR_NEW_FLOW, {
    traderIds: [transaction.getReceiverId()],
    message: messages.receiver.offerOutOfToleratedLimit
  })

  return TransactionsSessionManager
    .excludeTransactionFromParentTransactionsSession(transaction)
}

const handleQuoteCancelledAfterSend = transaction => {
  transaction.setStatus(TRANSACTION_STATUSES.CANCELLED)

  events.emit(eventNames.PREPARE_TRADERS_FOR_NEW_FLOW, {
    traderIds: [transaction.getReceiverId()],
    message: messages.receiver.freeToStartANewTrade
  })

  return TransactionsSessionManager
    .excludeTransactionFromParentTransactionsSession(transaction)
    .then(transactionsSession => transaction.save())
}

export default onEnterOfferDeclined
