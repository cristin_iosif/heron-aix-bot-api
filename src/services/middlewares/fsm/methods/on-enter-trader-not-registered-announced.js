import messages from '../../../../user-messages'

/**
 * Informs trader that he is not registered.
 * @param  {Object} session User's session.
 */
const onEnterTraderNotRegisteredAnnounced = function ({ session }) {
  session.outputMessage = messages.common.registrationRequired

  return Promise.resolve()
}

export default onEnterTraderNotRegisteredAnnounced
