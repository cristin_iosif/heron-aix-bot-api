const onEnterMiddlewareExit = function ({ session, sessionWrapper, next }) {
  if (session.isWrappedBySessionWrapper) {
    /* Unwrap the session wrapper from the session object to prevent saving it
    to db. */
    sessionWrapper.unwrap(session)
  }

  next()
}

export default onEnterMiddlewareExit
