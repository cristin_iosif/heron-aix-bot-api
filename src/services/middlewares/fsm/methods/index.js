import onEnterTraderNotRegisteredAnnounced from './on-enter-trader-not-registered-announced'
import onEnterMiddlewareExited from './on-enter-middleware-exited'
import onEnterFinancialInstrumentNotActiveAnnounced from './on-enter-financial-instrument-not-active-announced'
import onEnterRateRequestSent from './on-enter-rate-request-sent'
import onEnterTwoWayQuoteSent from './on-enter-two-way-quote-sent'
import onEnterOfferAccepted from './on-enter-offer-accepted'
import onEnterOfferDeclined from './on-enter-offer-declined'
import onEnterTradeRequestSent from './on-enter-trade-request-sent'
import onEnterTwoWayQuoteRefused from './on-enter-two-way-quote-refused'
import onEnterTradeCancelled from './on-enter-trade-cancelled'
import onEnterObjectionHandled from './on-enter-objection-handled'
import onEnterIcebergTradeDecision from './on-enter-iceberg-trade-decision'
import onEnterTradeOptionSpecified from './on-enter-trade-option-specified'

/**
 * Collects and exports all the fsm transition hook functions.
 * @param  {Object} args Extra fields to be set on each exported transition
 * function.
 * @return {Object}      Object with all the transition functions
 */
export default (...args) => ({
  // field names must match the structure "{timeTrigger+stateName}" like: onEnter + TwoWayQuoteRefused
  onEnterTraderNotRegisteredAnnounced: onEnterTraderNotRegisteredAnnounced.bind(this, ...args),
  onEnterMiddlewareExited: onEnterMiddlewareExited.bind(this, ...args),
  onEnterFinancialInstrumentNotActiveAnnounced: onEnterFinancialInstrumentNotActiveAnnounced.bind(this, ...args),
  onEnterRateRequestSent: onEnterRateRequestSent.bind(this, ...args),
  onEnterTwoWayQuoteSent: onEnterTwoWayQuoteSent.bind(this, ...args),
  onEnterOfferAccepted: onEnterOfferAccepted.bind(this, ...args),
  onEnterOfferDeclined: onEnterOfferDeclined.bind(this, ...args),
  onEnterTradeRequestSent: onEnterTradeRequestSent.bind(this, ...args),
  onEnterTwoWayQuoteRefused: onEnterTwoWayQuoteRefused.bind(this, ...args),
  onEnterTradeCancelled: onEnterTradeCancelled.bind(this, ...args),
  onEnterObjectionHandled: onEnterObjectionHandled.bind(this, ...args),
  onEnterIcebergTradeDecision: onEnterIcebergTradeDecision.bind(this, ...args),
  onEnterTradeOptionSpecified: onEnterTradeOptionSpecified.bind(this, ...args)
})
