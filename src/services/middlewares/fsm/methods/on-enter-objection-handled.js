import events, { eventNames } from '../../../events-system/event-emitter'
import helper from '../fsm-helper'

/**
 * Handles the case when a trader expresses an objection during the
 * conversation.
 */
const onEnterObjectionHandled = ({ session }) => {
  if (!session.outputMessage.length) {
    // send an hardcoded response message to the user
    session.outputMessage = [
      '[HARDCODED] Taking in consideration the market prices actually the bid' +
      ' is really good'
    ]
  }

  // get objection specific fields from the watson context
  let objectionTradePhase = helper.getObjectionTradePhase(session)
  let objectionObject = helper.getObjectionObject(session)

  console.log(
    `OBJECTION detected: objection = ${objectionTradePhase}; ` +
    `object = ${objectionObject}`
  )

  // TODO Remove this when it becomes clear how to handle objection
  events.emit(
    eventNames.CANCEL_TRANSACTIONS_SESSION,
    {
      transactionsSessionId: session.transactionsSessionId,
      isCancelledByInitiator: true
    }
  )
  session.watson.context = {}

  return Promise.resolve()
}

export default onEnterObjectionHandled
