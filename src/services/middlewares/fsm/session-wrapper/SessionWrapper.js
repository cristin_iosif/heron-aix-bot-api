import errors from '../../../common/errors'

class SessionWrapper {
  constructor () {
    this.wrapperFunctions = [
      { name: 'hasChainId', fn: this.hasChainId },
      { name: 'isInitiatorSession', fn: this.isInitiatorSession },
      { name: 'isReceiverSession', fn: this.isReceiverSession },
      { name: 'getChainId', fn: this.getChainId },
      { name: 'hasWatsonContext', fn: this.hasWatsonContext }
    ]
  }

  hasChainId () {
    return !!this.transactionsSessionChainId &&
      typeof this.transactionsSessionChainId === 'string'
  }

  isReceiverSession () {
    return !!this.activeTransaction &&
      typeof this.activeTransaction === 'object'
  }

  isInitiatorSession () {
    return this.activeTransaction === undefined ||
      this.activeTransaction == null
  }

  getChainId () {
    return this.transactionsSessionChainId
  }

  hasWatsonContext () {
    return this.watson &&
      this.watson.context &&
      typeof this.watson.context === 'object'
  }

  wrap (session) {
    if (!session || typeof session !== 'object') {
      throw errors.SessionNotDefinedError
    }

    this.wrapperFunctions.forEach(fnConfig => {
      this.assignFunction(session, fnConfig.name, fnConfig.fn)
    })

    session.isWrappedBySessionWrapper = true

    return session
  }

  unwrap (session) {
    if (!session || typeof session !== 'object') {
      throw errors.SessionNotDefinedError
    }

    this.wrapperFunctions.forEach(fnConfig => {
      this.unassignFunction(session, fnConfig.name, fnConfig.fn)
    })

    delete session.isWrappedBySessionWrapper
  }

  assignFunction (session, fnName, fn) {
    if (session.hasOwnProperty(fnName)) {
      throw errors.SessionFieldOverrideError(fnName)
    }

    session[fnName] = fn
  }

  unassignFunction (session, fnName) {
    if (
      !session.hasOwnProperty(fnName) ||
      typeof session[fnName] !== 'function'
    ) {
      throw errors.SessionFieldUnassignError(fnName, 'function')
    }

    delete session[fnName]
  }
}

export default SessionWrapper
