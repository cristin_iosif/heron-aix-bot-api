import { watsonConfig } from '../../../config'
import { USER_TYPES } from '../../../constants'
import { mockWatsonUpdateResponse } from './mocks'
import _sendMessageToWatson from '../../common/send-message-to-watson'
import helper from '../fsm/fsm-helper'

const watsonDeveloperCloud = require('watson-developer-cloud')
const watsonDebug = require('debug')(
  'botmaster:watson-conversation-middleware'
)
const watsonOptions = {
  settings: {
    username: watsonConfig.username,
    password: watsonConfig.password,
    url: watsonConfig.url,
    version: 'v1', // as of this writing (01 Apr 2017), only v1 is available
    version_date: '2017-05-26' // latest version-date as of this writing
  }
}

/**
 * Middleware responsible for sending requests to IBM Watson with the text
 * written by the user in the chat application. This request is made to discover
 * the user itent written in the text. The Watson response contains the intent
 * name, together with other variables which must be used to run and control the
 * trade flows.
 *
 * To be mounted on POST /bot-messages endpoint.
 *
 * @param {options} Middleware predefined options (Nothing at the moment).
 * @return {Function} The middleware function which should be mounted.
 */
const watsonCommunicationMiddleware = options => {
  console.log('Watson Communication Middleware')

  const {
    crypto: {
      initiatorWorkspaceId, // where to send the messages from the initiator trader
      receiverWorkspaceId // where to send the messages from the receiver traders
    }
  } = watsonConfig
  const watson = watsonDeveloperCloud.conversation(watsonOptions.settings)

  function watsonCommunicationMiddlewareFn (req, res, next) {
    // don't do this in unit test mode
    if (process.env.NODE_ENV === 'unit_test') {
      return next()
    }

    const session = req.session
    const body = req.body
    let message = body.message

    // if a watson communication error was already detected, skip this middleware
    if (session.isWatsonError) {
      return next()
    }

    if (!session) {
      throw new Error('Watson conversation ware needs to be used with SessionWare.')
    }
    if (!message) {
      watsonDebug('Got an update with no text, not sending it to Watson')

      return next()
    }
    const workspaceId = _getWorkspaceId(session)
    const commonWatsonParams = {
      workspaceId,
      watson
    }

    let promise = Promise.resolve(session.watson)
    let setContext = session.setContext && session.watson && session.watson.context // true if should send /set-context first

    if (setContext) {
      console.log('SET CONTEXT TO WATSON workspace:', workspaceId)
      console.log(session.watson.context)

      console.log('!!! USING /set_context')
      // make two calls, first with /set_context message to initiallize the context on watson
      promise = _sendMessageToWatson(Object.assign({}, commonWatsonParams, {
        context: session.watson.context,
        text: '/set_context'
      }))
        .catch(err => {
          console.log('Failed to send set_context message to Watson, ERR:', err)
          helper.watsonErrorHandler(err, session)
          session.isWatsonError = true
        })
    }

    promise
      .then(watsonUpdate => {
        if (setContext) { // if watsonUpdate come from a /set-context
          console.log('RECEIVED FROM /set_context of workspace:', workspaceId)
          console.log(watsonUpdate.context)
          console.log('#### Sending it with message:', message, '; to workspace:', workspaceId)

          session.setContext = null
        } else if (!watsonUpdate || !watsonUpdate.context) {
          console.log('NO CONTEXT FOUND, INITIALLIZING WATSON CONTEXT of workspace:', workspaceId)

          // send an empty text and context message to Watson (this is needed because that's how Watson works)
          return _sendMessageToWatson(Object.assign({}, commonWatsonParams, {
            context: {},
            text: ''
          }))
        }

        return Promise.resolve(watsonUpdate)
      })
      .then(watsonUpdate => {
        console.log('SENDING TO WATSON (message: ' + message + '), workspace:', workspaceId)
        console.log(watsonUpdate.context)

        // send the user text together with the existing/initialised/newly set context
        return _sendMessageToWatson(Object.assign({}, commonWatsonParams, {
          context: watsonUpdate.context,
          text: message
        }))
      })
      .then(watsonUpdate => {
        if (watsonUpdate.output.error) {
          throw watsonUpdate
        }

        // mock the response in case of missing Watson behaviour
        session.watson = mockWatsonUpdateResponse(
          message, watsonUpdate.context, watsonUpdate, session
        )

        next()
      })
      .catch(err => {
        console.log('Failed to send message to Watson, ERR:', err)
        helper.watsonErrorHandler(err, session)

        session.isWatsonError = true
        return next()
      })
  }

  // picks between the initiator and receiver workspaces base on the user type
  const _getWorkspaceId = session => {
    return session.userType === USER_TYPES.MARKET_MAKER
      ? session.receiverWorkspaceId : session.initiatorWorkspaceId
  }

  return watsonCommunicationMiddlewareFn
}

export default new watsonCommunicationMiddleware()
