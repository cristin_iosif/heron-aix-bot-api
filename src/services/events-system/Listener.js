import events from './event-emitter'

/**
 * "Abstract" class to be inherited by all action listeners.
 * It must receive the listened event name as constructor call parameter.
 * It provides the event listener register/deregister hooks.
 * listenerFn should be overwritten.
 * @type {[type]}
 */
class Listener {
  constructor (eventName) {
    this.LISTENED_EVENT = eventName
  }

  // this must be overwritten by the concrete listener callback function
  listenerFn () {}

  register () {
    events.addListener(this.LISTENED_EVENT, this.listenerFn)
  }

  deregister () {
    events.removeListener(this.LISTENED_EVENT, this.listenerFn)
  }
}

export default Listener
