import Listener from '../../Listener'
import events, { eventNames } from '../../event-emitter'
import {
  TransactionsSessionChainRepo,
  TransactionsSessionRepo,
  TransactionRepo
} from '../../../../repositories'
import { TransactionsSessionChainManager } from '../../../../managers'
import {
  TRANSACTIONS_SESSION_STATUSES,
  TRANSACTION_STATUSES
} from '../../../../constants'
import messages from '../../../../user-messages'

/**
 * Handles the case when the initiator wants to cancel the whole trading
 * operation.
 * @type {Class}
 */
class TransactionsSessionChainCancelListener extends Listener {
  constructor () {
    super(eventNames.CANCEL_CHAIN)
  }

  /**
   * Function to call when the event is captured. Receives an object with the
   * chainId of the chain which needs to be cancelled.
   */
  listenerFn ({ chainId }) {
    console.log('Executing CANCEL_CHAIN listener')
    let chain = null

    // finds the chain document
    TransactionsSessionChainRepo
      .findById(chainId)
      .then(chainEntity => {
        chain = chainEntity
        // stops all periodic check loops (eg. for the running transactions session)
        TransactionsSessionChainManager.stopAllPeriodicCheckLoops(chain)

        // closes all transaction sessions with CANCELLED status
        return TransactionsSessionRepo.updateByIds(
          chain.getTransactionsSessionIds(),
          { overallStatus: TRANSACTIONS_SESSION_STATUSES.CANCELLED }
        )
      })
      .then(() => {
        // closes all related transactions with CANCELLED status
        return TransactionRepo.updateAllTransactionsOfTransactionsSessionWithIds(
          chain.getTransactionsSessionIds(),
          { status: TRANSACTION_STATUSES.CANCELLED }
        )
      })
      .then(() => {
        return TransactionsSessionChainRepo.findChainReceiverTraderIds(chainId, {
          fromCancelledTransactions: true
        })
      })
      .then(receiverIds => {
        // announces the initiator and the receivers that the trade was cancelled
        let initiatorIds = [chain.getInitiatorId()]

        events.emit(eventNames.PREPARE_TRADERS_FOR_NEW_FLOW, {
          traderIds: initiatorIds,
          message: messages.initiator.transactionCancelled
        })

        events.emit(eventNames.PREPARE_TRADERS_FOR_NEW_FLOW, {
          traderIds: receiverIds,
          message: messages.receiver.transactionCancelled
        })
      })
  }
}

export default TransactionsSessionChainCancelListener
