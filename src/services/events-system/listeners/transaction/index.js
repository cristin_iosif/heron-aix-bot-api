/**
 * Collects and exports all listeners of the ./ directory.
 */
import TransactionConfirmationAsk from './transaction-confirmation-ask'
import TransactionConfirmationEmailSender from
  './transaction-confirmation-email-sender'

export default [
  new TransactionConfirmationAsk(),
  new TransactionConfirmationEmailSender()
]
