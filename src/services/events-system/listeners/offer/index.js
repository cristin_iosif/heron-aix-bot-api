/**
 * Collects and exports all listeners of the ./ directory.
 */
import spreadSender from './spread-sender'
import spreadMessageSender from './spread-message-sender'

export default [
  spreadSender,
  spreadMessageSender
]
