import { serviceDesk } from '../../../config'
let request = require('request-promise')

const serviceDeskIssueDescription = ({
  firstName,
  lastName,
  email,
  telegramId
}) => {
  return `Reporter info : \n` +
          ` First name : ${firstName} \n` +
          ` Last name : ${lastName} \n` +
          ` Email : ${email} \n` +
          ` Telegram ID : ${telegramId}`
}

export async function createServiceDeskIssue (firstName, lastName, email, telegramId, bugDescription) {
  let options = {
    method: 'POST',
    url: `${serviceDesk.serviceDeskMainUrl}/issue`,
    headers:
    {
      'Authorization': 'Basic ' + serviceDesk.serviceDeskBasicAuth,
      'Content-Type': 'application/json'
    },
    body:
    {
      fields: {
        project: {
          id: serviceDesk.projectId
        },
        summary: bugDescription,
        issuetype: {
          id: serviceDesk.issueTypeId
        },
        assignee: {
          name: serviceDesk.assigne
        },
        labels: [ 'AiXBotBug' ],
        description: serviceDeskIssueDescription(firstName, lastName, email, telegramId)
      }
    },
    json: true
  }
  let createdIssue = await request(options)
  return createdIssue
}

export async function addJsonAttachmentToServiceDeskIssue (createdIssue, jsonObject, jsonName) {
  let jsonFileContent = JSON.stringify(jsonObject)
  var options = {
    method: 'POST',
    url: `${serviceDesk.serviceDeskMainUrl}/issue/${createdIssue.id}/attachments`,
    headers: {
      'Authorization': 'Basic ' + serviceDesk.serviceDeskBasicAuth,
      'X-Atlassian-Token': 'no-check',
      'Accept': 'application/json',
      'content-type': 'multipart/form-data; boundary=----WebKitFormBoundary7MA4YWxkTrZu0gW'
    },
    formData: {
      file: {
        value: jsonFileContent,
        options: {
          filename: jsonName + Date.now() + '.json',
          contentType: null
        }
      }
    }
  }

  let createdAttachment = await request(options)
  return createdAttachment
}
