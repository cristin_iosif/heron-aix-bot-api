import { FinancialInstrumentRepo } from '../../repositories'
import CryptoCompareApi from '../crypto-compare-api-wrapper'
import { PRICES_INFO_UPDATE_PERIOD } from '../../constants'

/**
 * Class dedicated for acquiring and sharing the latest market prices for the
 * tracked financial instruments. This class is designed to run as a singleton
 * instance. It makes periodic GET calls to the cryptoCompare service to update
 * its prices.
 * @type {Class}
 */
class MarketPrices {
  /**
   * Creates a class instance, makes an initial fetch for prices, then refetches
   * them at a configurable periodic rate.
   * @return {[type]} [description]
   */
  constructor () {
    // currencies for which to get the prices
    this.trackedCurrencies = ['USD']
    // hastable with data for each cypto coin (referenced by coin identifer)
    this.cryptoCoins = {}
    // static average trade volume value. If this is set to Infinity it will disable iceberg question
    this.marketAverageTradeVolume = Infinity

    if (process.env.NODE_ENV === 'unit_test') {
      return
    }

    FinancialInstrumentRepo
      .findAllCrypto({ label: 1 }) // returns all financial instruments of crypto category
      .then(financialInstruments => {
        // all used crypto coin identifiers
        this.dbCryptoCoinsList = financialInstruments.map(e => e.label)

        return CryptoCompareApi.getCoinList()
      })
      .then(allAvailableCoinsList => {
        this.dbCryptoCoinsList.forEach(coinIdentifier => {
          let details = allAvailableCoinsList.Data[coinIdentifier]

          if (details) {
            this.cryptoCoins[coinIdentifier] = details
          } else {
            this.logCoinNotSupported(coinIdentifier)
          }
        })

        // do an initial fetch prices
        this.fetchPricesInfo()
        // refetch prices periodically
        setInterval(() => this.fetchPricesInfo(), PRICES_INFO_UPDATE_PERIOD)
      })
  }

  /**
   * Makes the call to cryptoCompare and calls the updatePricesInfo function
   * with the response data.
   * @return {undefined}
   */
  fetchPricesInfo () {
    let coinIdentifiers = Object.keys(this.cryptoCoins)

    CryptoCompareApi
      .getCurrentTradingInfo(coinIdentifiers, this.trackedCurrencies)
      .then(this.updatePricesInfo.bind(this))
  }

  /**
   * Save the prices data received from cryptoCompare, on the class scope by
   * calling updateCoinInfo on each received coin data.
   * @param  {Object} pricesInfo CryptoCompare response.
   * @return {undefined}
   */
  updatePricesInfo (pricesInfo) {
    if (pricesInfo.Response === 'Error') {
      return console.log(
        'Error while getting the current prices from crypto compare.',
        'The received message is:',
        pricesInfo.Message
      )
    }
    let rawPricesInfo = pricesInfo.RAW

    // Hastable with prices data for each cypto coin (referenced by coin identifer).
    this.currentPrices = this.currentPrices || {}
    Object.keys(rawPricesInfo).forEach(coinIdentifier => {
      this.updateCoinInfo(coinIdentifier, rawPricesInfo[coinIdentifier])
    })
  }

  /**
   * Updates specific coin current prices info, with the new coin data received.
   * @param  {String} coinIdentifier Identifies the coin.
   * @param  {Object} info           New prices data for the coin.
   * @return {undefined}
   */
  updateCoinInfo (coinIdentifier, info) {
    this.currentPrices[coinIdentifier] = info
  }

  /**
   * Returns the current price for the of the referenced coin.
   * @param  {String} coinIdentifier   Identifies the coin.
   * @param  {String} [currency='USD'] Currency to return the price in.
   * @return {Number}                  Current price of the referenced coin.
   */
  getCurrentPrice (coinIdentifier, currency = 'USD') {
    let info = this.getPricesInfo(coinIdentifier, currency)

    return info ? info.PRICE : 1
  }

  /**
   * Returns the lowest price of the day for the of the referenced coin.
   * @param  {String} coinIdentifier   Identifies the coin.
   * @param  {String} [currency='USD'] Currency to return the price in.
   * @return {Number}                  Today's lowest price of the referenced
   * coin.
   */
  getTodaysLowestPrice (coinIdentifier, currency = 'USD') {
    let info = this.getPricesInfo(coinIdentifier, currency)

    return info ? info.LOWDAY : 1
  }

  /**
   * Returns the highest price of the day for the of the referenced coin.
   * @param  {String} coinIdentifier   Identifies the coin.
   * @param  {String} [currency='USD'] Currency to return the price in.
   * @return {Number}                  Today's highest price of the referenced
   * coin.
   */
  getTodaysHighestPrice (coinIdentifier, currency = 'USD') {
    let info = this.getPricesInfo(coinIdentifier, currency)

    return info ? info.HIGHDAY : 1
  }

  /**
   * Returns all the prices information for the referenced coin.
   * @param  {String} coinIdentifier   Identifies the coin.
   * @param  {String} [currency='USD'] Currency to return the price in.
   * @return {Object}                  Current prices info of the referenced
   * coin.
   */
  getPricesInfo (coinIdentifier, currency) {
    let coinInfo = this.currentPrices && this.currentPrices[coinIdentifier]

    if (!coinInfo) {
      return this.logCoinPricesInfoMissing(coinInfo)
    }

    let pricesInfo = coinInfo[currency]

    if (!pricesInfo) {
      return this.logCoinPricesInfoCurrencyMissing(coinIdentifier, currency)
    }

    return pricesInfo
  }

  /**
   * Returns the average trade volume on the market.
   * @return {Number} The average trade volume on the market.
   */
  getMarketAverageTradeVolume () {
    return this.marketAverageTradeVolume
  }

  /**
   * Notify that the given coin identifier is not supported by the service.
   * @param  {String} coinIdentifier The coin identifier.
   */
  logCoinNotSupported (coinIdentifier) {
    console.log(
      'WARNING: Crypto Compare service does not support the following coin:',
      coinIdentifier
    )
  }

  /**
   * Notify that the prices information for the identified coin is not supported
   * by the service.
   * @param  {String} coinIdentifier The coin identifier.
   */
  logCoinPricesInfoMissing (coinIdentifier) {
    console.log(
      'ERR: Could not find the prices info of the following coin:',
      coinIdentifier
    )
  }

  /**
   * Notify that the prices information for the identified coin in the requested
   * currency, is not supported by the service.
   * @param  {String} coinIdentifier The coin identifier.
   * @param  {String} currency The currency requested.
   */
  logCoinPricesInfoCurrencyMissing (coinIdentifier, currency) {
    console.log(
      'ERR: Could not find the prices info of the following coin:',
      coinIdentifier,
      `converted to ${currency}.`
    )
  }
}

// expose a single instance
export default new MarketPrices()
