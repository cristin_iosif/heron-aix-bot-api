import { TransactionsSessionRepo } from '../../../repositories'
import { arrayHelper } from '../../common'

/**
 * Dedicated class for selecting the best bid transactions and best offer
 * transactions.
 * @type {Class}
 */
class BestBidOfferCalculator {
  /**
   * Calls repo function to get all bid values together with their transaction
   * id. Finds out the maximum bid value, extracts and returns only the
   * results which have the maximum bid value.
   * @param  {String} transactionsSessionId The transactions session id for
   * which to get the max bid transactions.
   * @return {Array}                      The max bid transactions of the
   * transactions session.
   */
  findSessionTransactionsWithMaxBid (transactionsSessionId) {
    return TransactionsSessionRepo
      .findSessionBidsInAscOrder(transactionsSessionId)
      .then(entities => {
        // each returned entity has {_id, bidValue}
        if (entities.length === 0) {
          return Promise.resolve([])
        }

        return arrayHelper.extractAllMax(entities, e => e.bidValue)
      })
  };

  /**
   * Calls repo function to get all offer values together with their transaction
   * id. Finds out the minimum offer value, extracts and returns only the
   * results which have the minimum offer value.
   * @param  {String} transactionsSessionId The transactions session id for
   * which to get the min offer transactions.
   * @return {Array}                      The min offer transactions of the
   * transactions session.
   */
  findSessionTransactionsWithMinOffer (transactionsSessionId) {
    return TransactionsSessionRepo
      .findSessionOffersInDescOrder(transactionsSessionId)
      .then(entities => {
        // each returned entity has {_id, offerValue}
        if (entities.length === 0) {
          return Promise.resolve([])
        }

        return arrayHelper.extractAllMin(entities, e => e.offerValue)
      })
  }
}

export default BestBidOfferCalculator
