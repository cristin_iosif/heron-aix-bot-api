import OffersService from './OffersService'
import fixturesLoader from '../../fixtures/fixtures-loader'
import { TransactionsSessionChain } from '../../api/transactions-session-chain'

describe('OffersService', () => {
  let inst = null
  let chain = null

  describe('instantiation', () => {
    test('should work without error', () => {
      expect(() => new OffersService()).not.toThrow()
    })
  })

  beforeEach(() => {
    inst = new OffersService()
  })

  describe('findChainBestOffers() function', () => {
    beforeEach(done => {
      fixturesLoader.loadFixtures(true, () => {
        TransactionsSessionChain
          .findOne()
          .then(chainEntity => {
            chain = chainEntity
            done()
          })
          .catch(err => {
            console.log(err)
            done(err)
          })
      })
    })

    test('should work as expected', done => {
      inst.getCurrentMarketPrice = jest.fn().mockImplementation(() => 11)

      inst
        .findChainBestOffers(chain)
        .then(result => {
          expect(result).toMatchObject({
            highestBid: expect.any(Number),
            highestBidTransactionIds: expect.any(Array),
            lowestOffer: expect.any(Number),
            lowestOfferTransactionIds: expect.any(Array),
            bidDeviationFromMarketPrice: expect.any(Number),
            offerDeviationFromMarketPrice: expect.any(Number)
          })

          expect(result.highestBid).toBe(10)
          expect(result.lowestOffer).toBe(12)
          expect(result.bidDeviationFromMarketPrice).toBeLessThan(0.1)
          expect(result.offerDeviationFromMarketPrice).toBeLessThan(0.1)

          done()
        })
    })

    test('should work when no market price exists', done => {
      inst.getCurrentMarketPrice = jest.fn().mockImplementation(() => 0)

      inst
        .findChainBestOffers(chain)
        .then(result => {
          expect(result).toMatchObject({
            highestBid: expect.any(Number),
            highestBidTransactionIds: expect.any(Array),
            lowestOffer: expect.any(Number),
            lowestOfferTransactionIds: expect.any(Array),
            bidDeviationFromMarketPrice: expect.any(Number),
            offerDeviationFromMarketPrice: expect.any(Number)
          })

          expect(result.highestBid).toBe(10)
          expect(result.lowestOffer).toBe(12)
          expect(result.bidDeviationFromMarketPrice).toBe(Infinity)
          expect(result.offerDeviationFromMarketPrice).toBe(Infinity)

          done()
        })
    })
  })
})
