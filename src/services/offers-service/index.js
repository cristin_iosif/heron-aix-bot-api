/**
 * Exposes a singleton instance of the offersService.
 * @type {OffersService}
 */
import OffersService from './OffersService'

export const offersService = new OffersService()

export default {
  offersService
}
