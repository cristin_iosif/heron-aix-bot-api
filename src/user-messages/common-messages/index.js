/**
 * This script defines common messages or functions that generate messages to
 * be displayed both into the receiver and initiator trader's chat.
 */

/* TODO Messages from user-messages/index.js should sorted during next
 refactoring and the common ones should be placed here.
 */

const financialInstrumentDescriptionMessageFn = ({
  financialInstrument,
  completedTransaction
}) => {
  if (financialInstrument.getLabel() === 'Iron Ore') {
    return `tonnes of ${completedTransaction.getMonth()} ` +
      `${completedTransaction.getYear()} ` +
      `$${completedTransaction.getValue()} strike Iron Ore ` +
      `${completedTransaction.getDealAction()}`
  } else {
    return financialInstrument.getName()
  }
}
const financialInstrumentNotSupportedMessage = 'Sorry, it looks like ' +
'the financial instrument you wish to trade is not currently supported.'

const registrationRequiredMessage = 'You need to be registered as a ' +
  'trader in order to use this bot. Just tap /start for details.'

export default {
  financialInstrumentDescriptionFn: financialInstrumentDescriptionMessageFn,
  financialInstrumentNotSupported: financialInstrumentNotSupportedMessage,
  registrationRequired: registrationRequiredMessage
}
