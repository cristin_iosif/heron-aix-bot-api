/**
 * This script defines messages or functions that generates messages to be shown
 * to traders into their chat.
 */
import initiatorMessages from './initiator-messages'
import receiverMessages from './receiver-messages'
import commonMessages from './common-messages'

export const spreadMessageFn = (bidValue, offerValue) =>
  `$${bidValue}/$${offerValue}`

export default Object.assign({
  spreadFn: spreadMessageFn
}, {
  common: commonMessages,
  initiator: initiatorMessages,
  receiver: receiverMessages
})
