/**
 * This script defines initiator messages or functions that generate messages to
 * be displayed into the receiver trader's chat.
 */

const tradeStatusUpdateMessageFn = ({
  totalVolume,
  remainingVolume,
  tradedVolume,
  financialInstrument
}) => {
  if (remainingVolume === 0) {
    return `You have traded all ${totalVolume} ${financialInstrument}.`
  }

  return `Update: You have traded ${tradedVolume} out of ${totalVolume} ` +
    `${financialInstrument}. You still have ${remainingVolume} ` +
    `${financialInstrument} available to trade.`
}

const noAvailableTradersMessage = 'It looks like there are no traders ' +
  'available to trade with. Cancelling for now. Please retry later.'

const transactionCancelledMessage = 'Ok, your transaction has been ' +
  'canceled. You can start another one at any time.'

const chainTimeoutReachedMessage = 'This trade has exceeded the maximum open ' +
  'time limit. You must start a new one, the current one will be closed.'

const icebergTransactionCongratulationMessageFn = ({
  remainingVolume,
  soldVolume,
  totalVolume,
  price,
  financialInstrument
}) => {
  if (!remainingVolume) {
    if (!price) {
      return `Trade closed: Your successfuly traded all your ${totalVolume} ` +
        `${financialInstrument}.`
    }

    return `Trade closed: Your traded all your ${totalVolume} ` +
      `${financialInstrument} successfuly, at a price limit of $${price}.`
  }

  if (!price) {
    return `Trade closed: You successfuly traded ${soldVolume} ` +
      `${financialInstrument}. You have ${remainingVolume} ` +
      `${financialInstrument} remaining from this iceberg to be traded next ` +
      `time.`
  }

  return `Trade closed: You successfuly traded ${soldVolume} ` +
    `${financialInstrument} at a price limit of $${price}. You have ` +
    `${remainingVolume} ${financialInstrument} remaining from this iceberg ` +
    `to be traded next time.`
}

const transactionCongratulationMessage = 'The transaction was ' +
  'confirmed. You will receive an email with the trade ' +
  'details.'

const transactionRefusedMessage = 'Trader refused the transaction. Sorry.'

const spreadMessageFn = ({
  isIcebergTrade,
  isBestSpread,
  smallestSpread,
  shardVolume,
  financialInstrument,
  numberOfSpreadsAlreadySent
}) => {
  if (isBestSpread) {
    return `Hello {trader}. This is the best offer I could get for you ` +
      `${smallestSpread}.`
  }

  if (numberOfSpreadsAlreadySent === 0) {
    if (!isIcebergTrade) {
      return `The first price we got is ${smallestSpread}. Still working for you.` +
        ` You can trade this, or you can wait until more traders respond.`
    }

    // TODO replace trader with {trader}
    return `Hi {trader}, I can show you a price of ${smallestSpread} for ` +
      `${shardVolume} ${financialInstrument}. What` +
      ` would you like to do?`
  } else {
    if (!isIcebergTrade) {
      return `More offers have gathered, the smallest spread for now is ` +
      `${smallestSpread}. You can trade this, or wait until more traders respond.`
    }

    // TODO replace trader with {trader}
    return `Hi {trader}, I can show you a price of ${smallestSpread} for ` +
    `${shardVolume} ${financialInstrument}. What ` +
    `would you like to do?`
  }
}

const tradeIntentionDeclinedMessageFn = tradeAction => {
  if (!tradeAction) {
    return 'Your intention was declined by the other trader. We are looking ' +
      'for other offers.'
  }

  return `Your ${tradeAction.toLowerCase()} was declined by the other trader.` +
    ` We are looking for other offers.`
}

export default {
  tradeStatusUpdateFn: tradeStatusUpdateMessageFn,
  noAvailableTraders: noAvailableTradersMessage,
  transactionCancelled: transactionCancelledMessage,
  chainTimeoutReached: chainTimeoutReachedMessage,
  icebergTransactionCongratulationFn: icebergTransactionCongratulationMessageFn,
  transactionCongratulation: transactionCongratulationMessage,
  transactionRefused: transactionRefusedMessage,
  spreadFn: spreadMessageFn,
  tradeIntentionDeclinedFn: tradeIntentionDeclinedMessageFn
}
