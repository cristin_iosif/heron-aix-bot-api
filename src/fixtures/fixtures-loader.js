import async from 'async'
import mongoose from '../services/mongoose'

import { Trader } from '../api/trader'
import { FinancialInstrument } from '../api/financial-instrument'
import { Transaction } from '../api/transaction'
import { TransactionsSession } from '../api/transactions-session'
import { TransactionsSessionChain } from '../api/transactions-session-chain'

import developmentData from './development'
import testData from './test'

/**
 * Fixtures data are loaded from src/fixtures/development or src/fixtures/test
 * directories. If you didn't set any fixtures data, please create the default
 * fixtures data files by copying each [.sample.js] file found in the data
 * directory, into individual [.js] files. (Yes, you only have to create a
 * new file with .sample removed). Any fixtures date provided in the non .sample
 * files will be local (ignored by git).
 * Advantage: Your trader accounts won't mess up with other developer's trader
 * accounts.
 */

function loadFixtures (runSilent, callback) {
  this.runSilent = runSilent

  !this.runSilent && console.log('Loading fixtures...\n')

  let fixtureLoaderCommands = null

  if (process.env.NODE_ENV === 'unit_test') {
    // use the fixture data from /test
    fixtureLoaderCommands = [
      dropEntireDatabase.bind(this),
      financialInstrumentsLoader.bind(this, testData.financialInstruments),
      traderLoader.bind(this, testData.traders),
      chainsLoader.bind(this, testData.chains),
      transactionsSessionLoader.bind(this, testData.transactionsSessions),
      transactionLoader.bind(this, testData.transactions),
      interDocumentMapsLoader.bind(this, testData.maps)
    ]
  } else {
    // use the fixtures data from /development
    fixtureLoaderCommands = [
      dropEntireDatabase.bind(this),
      financialInstrumentsLoader
        .bind(this, developmentData.financialInstruments),
      traderLoader.bind(this, developmentData.traders),
      interDocumentMapsLoader.bind(this, developmentData.maps)
    ]
  }

  // run each load fixture command in order
  async.eachSeries(
    fixtureLoaderCommands,
    function loadFixtureSet (fixturesLoader, callback) {
      fixturesLoader(callback)
    },
    callback
  )
}

function dropEntireDatabase (callback) {
  !this.runSilent && console.log('\nRemoving all collections from db:')

  let collections = Object.values(mongoose.connection.collections)

  async.map(
    collections,
    (collection, callback) => collection.remove(callback),
    callback
  )
}

function traderLoader (traders, callback) {
  !this.runSilent && console.log('\nLoading traders:')
  async.each(
    traders,
    (trader, callback) => {
      Trader
        .findOne({ telegramId: trader.telegramId }, (err, foundTrader) => {
          if (!foundTrader) {
            return Trader
              .create(trader)
              .then(() => callback())
              .catch(err => {
                console.log(err)
                callback(err)
              })
          }

          foundTrader
            .update(trader, { overwrite: true })
            .then(() => callback())
            .catch(err => {
              console.log(err)
              callback(err)
            })
        })
    },
    callback
  )
}

function financialInstrumentsLoader (financialInstruments, callback) {
  !this.runSilent && console.log('\nLoading financial instruments:')

  async.each(
    financialInstruments,
    (instrument, callback) => {
      FinancialInstrument
        .findOne({ label: instrument.label }, (err, financialInstrument) => {
          if (!financialInstrument) {
            return FinancialInstrument
              .create(instrument)
              .then(() => callback())
              .catch(err => {
                console.log(err)
                callback(err)
              })
          }

          financialInstrument
            .update(instrument, { overwrite: true })
            .then(() => callback())
            .catch(err => {
              console.log(err)
              callback(err)
            })
        })
    },
    callback
  )
}

function transactionLoader (transactions, callback) {
  !this.runSilent && console.log('\nLoading transactions:')

  async.each(
    transactions,
    (transactionData, callback) => {
      Transaction
        .create(transactionData)
        .then(() => callback())
        .catch(err => {
          console.log(err)
          callback(err)
        })
    },
    callback
  )
}

function transactionsSessionLoader (transactionsSessions, callback) {
  !this.runSilent && console.log('\nLoading transactions sessions:')

  async.each(
    transactionsSessions,
    (transactionsSessionData, callback) => {
      TransactionsSession
        .create(transactionsSessionData)
        .then(() => callback())
        .catch(err => {
          console.log(err)
          callback(err)
        })
    },
    callback
  )
}

function chainsLoader (chains, callback) {
  !this.runSilent && console.log('\nLoading chains:')

  async.each(
    chains,
    (chainData, callback) => {
      TransactionsSessionChain
        .create(chainData)
        .then(() => callback())
        .catch(err => {
          console.log(err)
          callback(err)
        })
    },
    callback
  )
}

function interDocumentMapsLoader (mapSets, callback) {
  !this.runSilent && console.log('\nCreating inter-document links:')

  async.each(
    Object.keys(mapSets),
    (mapSetFieldName, callback) => {
      let mapSet = mapSets[mapSetFieldName]

      switch (mapSetFieldName) {
        case 'traderFinancialInstrumentsMap':
          return mapFinancialInstrumentsToTraders.bind(this)(mapSet, callback)
        case 'transactionToTransactionsSessionMap':
          return mapTransactionsSessionsToTransactions.bind(this)(mapSet, callback)
        case 'transactionsSessionToChainMap':
          return mapChainToTransactionsSession.bind(this)(mapSet, callback)
        case 'financialInstrumentToChainMap':
          return mapFinancialInstrumentsToChain.bind(this)(mapSet, callback)
        default:
          return callback()
      }
    },
    callback
  )
}

function mapFinancialInstrumentsToTraders (maps, callback) {
  !this.runSilent && console.log('\nLinking financial instruments to traders:')

  async.each(maps, (map, callback) => {
    Trader.findOne(map.traderMatchCondition, (err, trader) => {
      if (err) {
        return callback(err)
      }
      FinancialInstrument.find(
        map.financialInstrumentMatchCondition,
        (err, instruments) => {
          if (err) {
            return callback(err)
          } else if (!trader) {
            return callback(new Error('Missing trader'))
          }
          trader.update({
            financialInstrumentsInUse: instruments.map(instrument => instrument._id)
          })
            .then(() => callback())
            .catch(err => {
              console.log(err)
              callback(err)
            })
        })
    })
  }, err => {
    callback()
  })
}

function mapFinancialInstrumentsToChain (maps, callback) {
  !this.runSilent && console.log('\nLinking financial instruments to chains:')

  async.each(maps, (map, callback) => {
    TransactionsSessionChain.findOne(map.chainMatchCondition, (err, chain) => {
      FinancialInstrument.findOne(
        map.financialInstrumentMatchCondition,
        (err, instrument) => {
          chain
            .update({ financialInstrumentId: instrument })
            .then(() => callback())
            .catch(err => {
              console.log(err)
              callback(err)
            })
        })
    })
  }, err => {
    callback()
  })
}

function mapTransactionsSessionsToTransactions (maps, callback) {
  !this.runSilent && console.log('\nLinking transactions sessions to transactions:')

  async.each(maps, (map, callback) => {
    TransactionsSession
      .findOne(
        map.transactionsSessionMatchCondition,
        (err, transactionsSession) => {
          Transaction
            .find(map.transactionMatchCondition)
            .then(transactions => {
              transactionsSession.transactions = []
              transactions.forEach(t => {
                t.transactionsSessionId = transactionsSession._id
                transactionsSession.transactions.push(t._id)
              })

              return Promise.all(transactions.map(t => t.save()))
            })
            .then(() => transactionsSession.save())
            .then(() => callback())
            .catch(err => {
              console.log(err)
              callback(err)
            })
        }
      )
  }, err => {
    callback()
  })
}

function mapChainToTransactionsSession (maps, callback) {
  !this.runSilent && console.log('\nLinking chains to transactions session:')

  async.each(maps, (map, callback) => {
    TransactionsSessionChain
      .findOne(
        map.chainMatchCondition,
        (err, chain) => {
          TransactionsSession
            .find(map.transactionsSessionMatchCondition)
            .then(sessions => {
              chain.transactionsSessionIds = []
              sessions.forEach(ts => {
                ts.transactionsSessionChainId = chain._id
                chain.transactionsSessionIds.push(ts._id)
              })

              return Promise.all(sessions.map(ts => ts.save()))
            })
            .then(() => chain.save())
            .then(() => callback())
            .catch(err => {
              console.log(err)
              callback(err)
            })
        }
      )
  }, err => {
    callback()
  })
}

export default {
  loadFixtures
}
