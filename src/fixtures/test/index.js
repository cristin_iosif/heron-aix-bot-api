import traders from './traders'
import financialInstruments from './financial-instruments'
import maps from './inter-document-map'
import transactions from './transactions'
import transactionsSessions from './transactions-sessions'
import chains from './chains'

export default {
  traders,
  financialInstruments,
  transactionsSessions,
  transactions,
  maps,
  chains
}
