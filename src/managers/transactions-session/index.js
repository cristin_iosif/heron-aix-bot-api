import differenceWith from 'lodash/differenceWith'
import {
  TransactionsSessionRepo,
  TransactionsSessionChainRepo,
  FinancialInstrumentRepo
} from '../../repositories'
import { stringsToObjectIds } from '../../services/common/common'
import comparators from '../../services/common/comparators'

const findAllDependenciesOfTransactionsSessionWithId = id => {
  let transactionsSession = null
  let chain = null
  let financialInstrument = null

  return TransactionsSessionRepo
    .findById(id)
    .then(transactionsSessionEntity => {
      transactionsSession = transactionsSessionEntity

      return TransactionsSessionChainRepo
        .findById(transactionsSession.getChainId())
    })
    .then(chainEntity => {
      chain = chainEntity

      return FinancialInstrumentRepo.findById(chain.getFinancialInstrumentId())
    })
    .then(financialInstrumentEntity => {
      financialInstrument = financialInstrumentEntity

      return Promise.resolve({
        chain,
        transactionsSession,
        financialInstrument
      })
    })
    .catch(err => {
      console.log(
        'Error in findAllDependenciesOfTransactionsSessionWithId, ERR:', err
      )
    })
}

const excludeTransactionIdsFromTransactionsSession = (
  transactionsSession,
  transactionIds
) => {
  let existingIds = stringsToObjectIds(transactionsSession.transactions)
  let specifiedIds = stringsToObjectIds(transactionIds)
  let remainingIds = differenceWith(
    existingIds,
    specifiedIds,
    comparators.objectId
  )

  transactionsSession.setTransactionIds(
    remainingIds.map(objectId => objectId.valueOf())
  )

  return transactionsSession
}

const excludeTransactionIdsFromTransactionsSessionWithId = (
  transactionsSessionId,
  transactionIds
) => {
  return TransactionsSessionRepo.findById(transactionsSessionId)
    .then(transactionsSession => {
      excludeTransactionIdsFromTransactionsSession(
        transactionsSession,
        transactionIds
      )

      return transactionsSession.save()
    })
}

const excludeTransactionFromParentTransactionsSession = transaction => {
  return excludeTransactionIdsFromTransactionsSessionWithId(
    transaction.getTransactionsSessionId(),
    [ transaction.getId() ]
  )
}

export default {
  findAllDependenciesOfTransactionsSessionWithId,

  excludeTransactionIdsFromTransactionsSession,
  excludeTransactionIdsFromTransactionsSessionWithId,
  excludeTransactionFromParentTransactionsSession
}
