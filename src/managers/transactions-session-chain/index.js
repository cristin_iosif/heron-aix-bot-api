import events, { eventNames } from '../../services/events-system/event-emitter'
import {
  TransactionsSessionRepo,
  TransactionsSessionChainRepo,
  FinancialInstrumentRepo
} from '../../repositories'

const findAllDependenciesOfChainWithId = id => {
  let chain = null
  let financialInstrument = null

  return TransactionsSessionChainRepo
    .findById(id)
    .then(chainEntity => {
      chain = chainEntity

      return FinancialInstrumentRepo.findById(chain.getFinancialInstrumentId())
    })
    .then(financialInstrumentEntity => {
      financialInstrument = financialInstrumentEntity

      return Promise.resolve({
        financialInstrument,
        chain
      })
    })
}

const setFirstOfferSentAtIfNecessary = transactionsSessionId => {
  TransactionsSessionRepo
    .findById(transactionsSessionId, { transactionsSessionChainId: 1 })
    .then(transactionsSession => {
      return TransactionsSessionChainRepo.findById(
        transactionsSession.transactionsSessionChainId
      )
    })
    .then(chain => {
      if (!chain.getFirstOfferSentAt()) {
        chain.setFirstOfferSentAt(new Date())
        chain.save()
      }
    })
}

const stopAllPeriodicCheckLoops = chain => {
  return TransactionsSessionRepo
    .findByIds(
      chain.getTransactionsSessionIds(),
      { periodicCheckIntervalRef: 1 }
    )
    .then(transactionSessions => {
      let refs = transactionSessions.map(s => s.periodicCheckIntervalRef)

      refs.map(ref => {
        events.emit(eventNames.REMOVE_INTERVAL_LOOP, { intervalRef: ref })
      })

      return Promise.resolve()
    })
}

export default {
  findAllDependenciesOfChainWithId,
  setFirstOfferSentAtIfNecessary,
  stopAllPeriodicCheckLoops
}
