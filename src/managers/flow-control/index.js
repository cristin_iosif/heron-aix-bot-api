import periodicCheck from './periodic-check'

const findAllFlowControlParameters = (transactionsSession, chain) => {
  return Promise
    .all([
      periodicCheck.countEntireChainOffersReceived(transactionsSession, chain),
      periodicCheck.countEntireChainPossibleOffers(transactionsSession, chain),
      periodicCheck.countTransactionsSessionOffersReceived(transactionsSession, chain),
      periodicCheck.countEntireChainTradeOptionAnswered(transactionsSession, chain),
      periodicCheck.countEntireChainConfirmationsAsked(transactionsSession, chain)
    ])
    .then(([
      chainReceivedOffersCount,
      chainPossibleOffersCount,
      transactionsSessionReceivedOffersCount,
      repliedAnswersCount,
      confirmationsAskedCount
    ]) => {
      return Promise.resolve({
        chainReceivedOffersCount,
        chainPossibleOffersCount,
        transactionsSessionReceivedOffersCount,
        repliedAnswersCount,
        confirmationsAskedCount
      })
    })
    .catch(err => {
      console.log('Failed to findAllFlowControlParameters! ERR:', err)
    })
}

export default Object.assign(
  {
    findAllFlowControlParameters
  },
  periodicCheck
)
