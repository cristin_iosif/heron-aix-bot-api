import TransactionManager from './transaction'
import TransactionsSessionManager from './transactions-session'
import TransactionsSessionChainManager from './transactions-session-chain'

export const findAsManyDependenciesOf = params => {
  if (params.transactionId) {
    return TransactionManager
      .findAllDependenciesOfTransactionWithId(params.transactionId)
  } else if (params.transactionsSessionId) {
    return TransactionsSessionManager
      .findAllDependenciesOfTransactionsSessionWithId(
        params.transactionsSessionId
      )
  } else if (params.chainId) {
    return TransactionsSessionChainManager
      .findAllDependenciesOfChainWithId(params.chainId)
  }

  return Promise.reject(new Error(
    'Could not find any of transactionId, transactionsSessionId and chainId!'
  ))
}

export default {
  findAsManyDependenciesOf
}
