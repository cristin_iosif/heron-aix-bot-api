import { Trader } from '../../api/trader'
import { SessionInitializationData } from
  '../../api/session-initialization-data'

const updateForTraderWithId = (traderId, newData) => {
  return Trader
    .findById(traderId, { telegramId: 1 })
    .then(trader => {
      console.log('Updating', traderId, 'initialization data with', newData)

      return SessionInitializationData
        .update(
          { traderTelegramId: trader.getTelegramId() },
          {
            traderTelegramId: trader.getTelegramId(), // in case data does not exist yet
            initiallizationData: newData
          },
          { upsert: true }
        ).exec()
    })
    .catch(err => {
      console.log('Could not find trader with id ', traderId, ', ERR', err)
    })
}

const deleteForTradeWithId = traderId => {
  return Trader
    .findById(traderId, { telegramId: 1 })
    .then(trader => {
      console.log('Deleting', traderId, 'initialization data')

      return SessionInitializationData
        .remove({ traderTelegramId: trader.getTelegramId() })
        .exec()
    })
    .catch(err => {
      console.log('Could not find trader with id ', traderId, ', ERR', err)
    })
}

export default {
  updateForTraderWithId,
  deleteForTradeWithId
}
