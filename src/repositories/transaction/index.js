import { Transaction } from '../../api/transaction'
import { TransactionsSession } from '../../api/transactions-session'
import {
  CLOSED_TRANSACTION_STATUSES,
  ACTIVE_TRANSACTION_STATUSES,
  TRANSACTION_STATUSES
} from '../../constants'

const findById = (id, projection = null) => {
  return Transaction.findById(id, projection)
}

const findByIds = (transactionIds, projection) => {
  return Transaction.find(
    { _id: { $in: transactionIds } },
    projection
  )
}

const findNotClosedByIds = (transactionIds, projection) => {
  return Transaction.find(
    {
      _id: { $in: transactionIds },
      status: { $not: { $in: CLOSED_TRANSACTION_STATUSES } }
    },
    projection
  )
}

const findNotActiveByIds = (transactionIds, projection) => {
  return Transaction.find(
    {
      _id: { $in: transactionIds },
      status: { $not: { $in: ACTIVE_TRANSACTION_STATUSES } }
    },
    projection
  )
}

const findNotClosedAndNotDecidedByIds = (transactionIds, projection) => {
  return Transaction.find(
    {
      _id: { $in: transactionIds },
      status: { $not: { $in: CLOSED_TRANSACTION_STATUSES.concat(
        TRANSACTION_STATUSES.ACCEPTED,
        TRANSACTION_STATUSES.DECLINED
      ) } }
    },
    projection
  )
}

const findCancelledByIds = (transactionIds, projection) => {
  return Transaction.find(
    {
      _id: { $in: transactionIds },
      status: TRANSACTION_STATUSES.CANCELLED
    },
    projection
  )
}

const findOpenByIds = (transactionIds, projection) => {
  return Transaction.find(
    {
      _id: { $in: transactionIds },
      status: TRANSACTION_STATUSES.OPEN
    },
    projection
  )
}

const findTransactionsSortedDescByBidValue = (matchCondition = {}, projection = null) => {
  return Transaction
    .find(matchCondition, projection)
    .sort({ bidValue: -1 })
}

const findTransactionsSortedAscByOfferValue = (matchCondition = {}, projection = null) => {
  return Transaction
    .find(matchCondition, projection)
    .sort({ offerValue: 1 })
}

const findAcceptedTransactionsOfChainWithId =
  (id, projection = null) => {
    return TransactionsSession
      .find({ transactionsSessionChainId: id }, { transactions: 1 })
      .then(sessions => {
        let transactionIds = []

        sessions.forEach(session => {
          transactionIds = transactionIds.concat(session.getTransactionIds())
        })

        return Transaction.find(
          {
            _id: { $in: transactionIds },
            status: TRANSACTION_STATUSES.ACCEPTED
          }
        )
      })
  }

const findFistTransactionWithSpecifiedStatusOfChainWithId =
  (id, status, projection = null) => {
    return TransactionsSession
      .find({ transactionsSessionChainId: id }, { transactions: 1 })
      .then(sessions => {
        let transactionIds = []

        sessions.forEach(session => {
          transactionIds = transactionIds.concat(session.getTransactionIds())
        })

        return Transaction
          .find({
            _id: { $in: transactionIds },
            status: status
          })
          .sort({ createdAt: 1 })
          .limit(1)
          .then(transactions => Promise.resolve(transactions[0]))
      })
  }

const findFirstAcceptedTransactionOfChainWithId = (id, projection = null) => {
  return findFistTransactionWithSpecifiedStatusOfChainWithId(
    id,
    TRANSACTION_STATUSES.ACCEPTED
  )
}

const findFirstDeclinedTransactionOfChainWithId = (id, projection = null) => {
  return findFistTransactionWithSpecifiedStatusOfChainWithId(
    id,
    TRANSACTION_STATUSES.DECLINED
  )
}

const count = condition => {
  return Transaction.count(condition)
}

const countTransactionsOfSessionsWithIds = ids => {
  return Transaction.count({ transactionsSessionId: { $in: ids } })
}

const updateById = (transactionId, updatedFields) => {
  return Transaction.update(
    { _id: transactionId },
    updatedFields
  ).exec()
}

const updateByIds = (transactionIds, updatedFields) => {
  return Transaction.update(
    { _id: { $in: transactionIds } },
    updatedFields,
    { multi: true }
  ).exec()
}

const updateAllActiveTransactionsOfTransactionsSessionWithIds = (
  transactionsSessionIds = [],
  updatedFields = null
) => {
  return Transaction.update(
    {
      transactionsSessionId: { $in: transactionsSessionIds },
      status: { $in: ACTIVE_TRANSACTION_STATUSES }
    },
    updatedFields,
    { multi: true }
  ).exec()
}

const updateAllTransactionsOfTransactionsSessionWithIds = (
  transactionsSessionIds = [],
  updatedFields = null
) => {
  return Transaction.update(
    { transactionsSessionId: { $in: transactionsSessionIds } },
    updatedFields,
    { multi: true }
  ).exec()
}

export default {
  findById,
  findByIds,
  findNotClosedByIds,
  findNotClosedAndNotDecidedByIds,
  findNotActiveByIds,
  findCancelledByIds,
  findOpenByIds,
  findTransactionsSortedDescByBidValue,
  findTransactionsSortedAscByOfferValue,
  findAcceptedTransactionsOfChainWithId,
  findFirstAcceptedTransactionOfChainWithId,
  findFirstDeclinedTransactionOfChainWithId,

  count,
  countTransactionsOfSessionsWithIds,

  updateById,
  updateByIds,
  updateAllTransactionsOfTransactionsSessionWithIds,
  updateAllActiveTransactionsOfTransactionsSessionWithIds
}
