import { success } from '../../services/response/'
import { GroupChat } from '.'
import moment from 'moment'
import numeral from 'numeral'
import marketPrices from '../../services/market-prices'
import CardsApi from '../../services/cards-api-wrapper'

export const create = ({ bodymen: { body } }, res, next) =>
  GroupChat.create(body)
    .then((groupChat) => groupChat.view(true))
    .then(success(res, 201))
    .catch(next)

export const createInfoCard = ({ bodymen: { body } }, res, next) =>
  GroupChat.create(body)
    .then((groupChat) => {
      const coin = 'BTC'
      const currency = 'USD'
      const currencySymbol = '$'
      const priceFormat = '0,0.00'

      let currentPrice = numeral(
        marketPrices.getCurrentPrice(coin, currency)
      ).format(priceFormat)
      let lowestPrice = numeral(
        marketPrices.getTodaysLowestPrice(coin, currency)
      ).format(priceFormat)
      let highestPrice = numeral(
        marketPrices.getTodaysHighestPrice(coin, currency)
      ).format(priceFormat)

      return CardsApi.getInfoCardWithContent('crypto_rates', {
        coin: coin,
        coinQuantity: 1,
        currency: currency,
        currencySymbol: currencySymbol,
        currentPrice: currentPrice,
        dayLowPrice: lowestPrice,
        dayHighPrice: highestPrice,
        now: moment().format('LTS')
      })
        .then(infoCard => {
          console.log('CARD IMAGE url received, sending it to botmaster.')

          let botmasterMessage = JSON.stringify({
            imgUrl: infoCard.url,
            coin: infoCard.content.coin
          })

          return botmasterMessage
        })
        .catch(err => {
          console.log('Card generate failed with, ERR', err)

          return JSON.stringify({
            message: [
              `Current price: ${currencySymbol}${currentPrice}`,
              `Today's lowest: ${currencySymbol}${lowestPrice}`,
              `Today's highest: ${currencySymbol}${highestPrice}`
            ]
          })
        })
    })
    .then(success(res, 201))
    .catch(next)
