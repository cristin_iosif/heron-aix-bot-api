import mongoose, { Schema } from 'mongoose'

const groupChatSchema = new Schema({
  message: {
    type: String
  },
  group_name: {
    type: String
  }
}, {
  timestamps: true,
  toJSON: {
    virtuals: true,
    transform: (obj, ret) => { delete ret._id }
  }
})

groupChatSchema.methods = {
  view (full) {
    const view = {
      // simple view
      id: this.id,
      message: this.message,
      group_name: this.group_name,
      createdAt: this.createdAt,
      updatedAt: this.updatedAt
    }

    return full ? {
      ...view
      // add properties for a full view
    } : view
  }
}

const model = mongoose.model('GroupChat', groupChatSchema)

export const schema = model.schema
export default model
