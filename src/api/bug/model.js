import mongoose, { Schema } from 'mongoose'

const bugSchema = new Schema({
  bug_description: {
    type: String
  },
  watsonContext: {
    type: Object
  },
  session: {
    type: Object
  },
  conversation_id: {
    type: String
  },
  user_telegram_id: {
    type: String
  }
}, {
  timestamps: true,
  toJSON: {
    virtuals: true,
    transform: (obj, ret) => { delete ret._id }
  }
})

bugSchema.methods = {
  view (full) {
    const view = {
      // simple view
      id: this.id,
      bug_description: this.bug_description,
      conversation_id: this.conversation_id,
      user_telegram_id: this.user_telegram_id,
      watsonContext: this.watsonContext,
      session: this.session,
      createdAt: this.createdAt,
      updatedAt: this.updatedAt
    }

    return full ? {
      ...view
      // add properties for a full view
    } : view
  }
}

const model = mongoose.model('Bug', bugSchema)

export const schema = model.schema
export default model
