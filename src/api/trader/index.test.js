import request from 'supertest'
import { masterKey, apiRoot } from '../../config'
import { signSync } from '../../services/jwt'
import express from '../../services/express'
import routes, { Trader } from '.'

const app = () => express(apiRoot, routes)

let trader, otherTrader, traderSession

beforeEach(async () => {
  trader = await Trader.create({ telegramId: '123456789', password: '123456' })
  otherTrader = await Trader.create({
    telegramId: '123456787',
    password: '123456'
  })
  traderSession = signSync(trader.id)
})

test('GET /traders/me 200 (user)', async () => {
  const { status, body } = await request(app())
    .get(apiRoot + '/me')
    .set({ authorization: `Bearer ${traderSession}` })
  expect(status).toBe(200)
  expect(typeof body).toBe('object')
  expect(body.id).toBe(trader.id)
})

test('GET /traders/me 401', async () => {
  const { status } = await request(app())
    .get(apiRoot + '/me')
  expect(status).toBe(401)
})

test('POST /traders 401 (master)', async () => {
  const { status } = await request(app())
    .post(apiRoot)
    .send({ telegramId: '123456788', password: '123456' })
  expect(status).toBe(401)
})

test('POST /traders 201 (master)', async () => {
  const { status, body } = await request(app())
    .post(apiRoot)
    .send({ telegramId: '123456788', password: '123456' })
    .set({ authorization: `Bearer ${masterKey}` })
  expect(status).toBe(201)
  expect(typeof body).toBe('object')
  expect(typeof body.trader).toBe('object')
  expect(typeof body.token).toBe('string')
  expect(body.trader.telegramId).toBe('123456788')
})

test('POST /traders 201 (master)', async () => {
  const { status, body } = await request(app())
    .post(apiRoot)
    .send({
      telegramId: '123456788',
      password: '123456',
      firstName: 'Johny',
      lastName: 'Test',
      companyName: 'home',
      traderType: 'test',
      financialInstrumentsInUse: []
    })
    .set({ authorization: `Bearer ${masterKey}` })
    .set('Accept', 'application/json')
  expect(status).toBe(201)
  expect(typeof body).toBe('object')
  expect(typeof body.trader).toBe('object')
  expect(body.trader).toEqual(expect.objectContaining({
    id: expect.any(String),
    telegramId: '123456788',
    firstName: 'Johny',
    lastName: 'Test',
    companyName: 'home',
    traderType: 'test',
    financialInstrumentsInUse: expect.any(Array),
    createdAt: expect.any(String),
    updatedAt: expect.any(String)
  }))
  expect(typeof body.token).toBe('string')
  expect(body.trader.telegramId).toBe('123456788')
})

test('POST /traders 401 (master) - duplicated telegramId', async () => {
  const { status } = await request(app())
    .post(apiRoot)
    .send({ telegramId: '123456789', password: '123456' })
  expect(status).toBe(401)
})

test('POST /traders 409 (master) - duplicated telegramId', async () => {
  const { status, body } = await request(app())
    .post(apiRoot)
    .send({ telegramId: '123456789', password: '123456' })
    .set({ authorization: `Bearer ${masterKey}` })
  expect(status).toBe(409)
  expect(typeof body).toBe('object')
  expect(body.valid).toBe(false)
  expect(body.param).toBe('telegramId')
})

test('POST /traders 401 (master) - invalid telegramId', async () => {
  const { status } = await request(app())
    .post(apiRoot)
    .send({ telegramId: '1', password: '123456' })
  expect(status).toBe(401)
})

test('POST /traders 400 (master) - invalid telegramId', async () => {
  const { status, body } = await request(app())
    .post(apiRoot)
    .send({ telegramId: '1', password: '123456' })
    .set({ authorization: `Bearer ${masterKey}` })
  expect(status).toBe(400)
  expect(typeof body).toBe('object')
  expect(body.valid).toBe(false)
  expect(body.param).toBe('telegramId')
})

test('POST /traders 401 (master) - missing telegramId', async () => {
  const { status } = await request(app())
    .post(apiRoot)
    .send({ password: '123456' })
  expect(status).toBe(401)
})

test('POST /traders 400 (master) - missing telegramId', async () => {
  const { status, body } = await request(app())
    .post(apiRoot)
    .send({ password: '123456' })
    .set({ authorization: `Bearer ${masterKey}` })
  expect(status).toBe(400)
  expect(typeof body).toBe('object')
  expect(body.valid).toBe(false)
  expect(body.param).toBe('telegramId')
})

test('POST /traders 401 (master) - invalid password', async () => {
  const { status } = await request(app())
    .post(apiRoot)
    .send({ telegramId: '123456788', password: '123' })
  expect(status).toBe(401)
})

test('POST /traders 400 (master) - invalid password', async () => {
  const { status, body } = await request(app())
    .post(apiRoot)
    .send({ telegramId: '123456788', password: '123' })
    .set({ authorization: `Bearer ${masterKey}` })
  expect(status).toBe(400)
  expect(typeof body).toBe('object')
  expect(body.valid).toBe(false)
  expect(body.param).toBe('password')
})

test('POST /traders 401 (master) - missing password', async () => {
  const { status } = await request(app())
    .post(apiRoot)
    .send({ telegramId: '123456788' })
  expect(status).toBe(401)
})

test('POST /traders 400 (master) - missing password', async () => {
  const { status, body } = await request(app())
    .post(apiRoot)
    .send({ telegramId: '123456788' })
    .set({ authorization: `Bearer ${masterKey}` })
  expect(status).toBe(400)
  expect(typeof body).toBe('object')
  expect(body.valid).toBe(false)
  expect(body.param).toBe('password')
})

test('PUT /traders/me 401 (user)', async () => {
  const { status } = await request(app())
    .put(apiRoot + '/me')
    .send({ firstName: 'John' })
  expect(status).toBe(401)
})

test('PUT /traders/me 200 (user)', async () => {
  const { status, body } = await request(app())
    .put(apiRoot + '/me')
    .send({ firstName: 'John' })
    .set({ authorization: `Bearer ${traderSession}` })
  expect(status).toBe(200)
  expect(typeof body).toBe('object')
  expect(body.firstName).toBe('John')
})

test('PUT /traders/me 401', async () => {
  const { status } = await request(app())
    .put(apiRoot + '/me')
    .send({ name: 'test' })
  expect(status).toBe(401)
})

test('PUT /traders/me 200 (user)', async () => {
  const { status, body } = await request(app())
    .put(`${apiRoot}/me`)
    .send({
      companyName: 'x',
      financialInstrumentsInUse: [],
      firstName: 'e',
      lastName: 'd',
      traderType: 'o'
    })
    .set({ authorization: `Bearer ${traderSession}` })
  expect(status).toBe(200)
  expect(typeof body).toBe('object')
  expect(body.companyName).toBe('x')
  expect(body.firstName).toBe('e')
  expect(body.lastName).toBe('d')
  expect(body.traderType).toBe('o')
  expect(body.financialInstrumentsInUse).toEqual(expect.any(Array))
})

test('PUT /traders/:id 401 (user)', async () => {
  const { status } = await request(app())
    .put(`${apiRoot}/${otherTrader.id}`)
    .send({ firstName: 'John' })
    .set({ authorization: `Bearer ${traderSession}` })
  expect(status).toBe(401)
})

test('PUT /traders/:id 404 (admin)', async () => {
  const { status } = await request(app())
    .put(apiRoot + '/123456789098765432123456')
    .send({ firstName: 'test' })
    .set({ authorization: `Bearer ${traderSession}` })
  expect(status).toBe(404)
})

const passwordMatch = async (password, userId) => {
  const user = await Trader.findById(userId)
  return !!await user.authenticate(password)
}

test('PUT /traders/me/password 200 (user)', async () => {
  const { status, body } = await request(app())
    .put(apiRoot + '/me/password')
    .auth('123456787', '123456')
    .send({ password: '654321' })
  expect(status).toBe(200)
  expect(typeof body).toBe('object')
  expect(body.telegramId).toBe('123456787')
  expect(await passwordMatch('654321', body.id)).toBe(true)
})

test('PUT /traders/me/password 400 (user) - invalid password', async () => {
  const { status, body } = await request(app())
    .put(apiRoot + '/me/password')
    .auth('123456787', '123456')
    .send({ password: '321' })
  expect(status).toBe(400)
  expect(typeof body).toBe('object')
  expect(body.valid).toBe(false)
  expect(body.param).toBe('password')
})

test('PUT /traders/me/password 401 (user) - invalid authentication method', async () => {
  const { status } = await request(app())
    .put(apiRoot + '/me/password')
    .send({ password: '654321' })
    .set({ authorization: `Bearer ${traderSession}` })
  expect(status).toBe(401)
})

test('PUT /traders/me/password 401', async () => {
  const { status } = await request(app())
    .put(apiRoot + '/me/password')
    .send({ password: '654321' })
    .set({ authorization: `Bearer ${traderSession}` })
  expect(status).toBe(401)
})

test('PUT /traders/:id/password 200 (user)', async () => {
  const { status, body } = await request(app())
    .put(`${apiRoot}/${trader.id}/password`)
    .auth('123456789', '123456')
    .send({ password: '654321' })
  expect(status).toBe(200)
  expect(typeof body).toBe('object')
  expect(await passwordMatch('654321', body.id)).toBe(true)
})

test('PUT /traders/:id/password 400 (user) - invalid password', async () => {
  const { status, body } = await request(app())
    .put(`${apiRoot}/${trader.id}/password`)
    .auth('123456789', '123456')
    .send({ password: '321' })
  expect(status).toBe(400)
  expect(typeof body).toBe('object')
  expect(body.valid).toBe(false)
  expect(body.param).toBe('password')
})

test('PUT /traders/:id/password 401 (user) - invalid authentication method', async () => {
  const { status } = await request(app())
    .put(`${apiRoot}/${trader.id}/password`)
    .send({ password: '654321' })
    .set({ authorization: `Bearer ${traderSession}` })
  expect(status).toBe(401)
})

test('PUT /traders/:id/password 401', async () => {
  const { status } = await request(app())
    .put(`${apiRoot}/${trader.id}/password`)
    .send({ password: '654321' })
  expect(status).toBe(401)
})

test('PUT /traders/:id/password 404 (user)', async () => {
  const { status } = await request(app())
    .put(apiRoot + '/123456789098765432123456/password')
    .auth('123456789', '123456')
    .send({ password: '654321' })
  expect(status).toBe(404)
})

test('DELETE /traders/:id 401 (user)', async () => {
  const { status } = await request(app())
    .delete(`${apiRoot}/${trader.id}`)
  expect(status).toBe(401)
})
