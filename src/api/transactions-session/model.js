import mongoose, { Schema } from 'mongoose'

const transactionsSessionSchema = new Schema({
  transactions: {
    type: Array
  },
  overallStatus: {
    type: String
  },
  initiatorTraderId: {
    type: String
  },
  initiatorTraderSessionId: {
    type: String
  },
  startedAt: {
    type: Date
  },
  expiresAt: {
    type: Date
  },
  bidVolume: {
    type: Number
  },
  highestBid: {
    type: Number
  },
  askedBid: {
    type: Number
  },
  highestBidTransactionId: {
    type: String
  },
  lowestOffer: {
    type: Number
  },
  askedOffer: {
    type: Number
  },
  lowestOfferTransactionId: {
    type: String
  },
  requestMonth: {
    type: String
  },
  requestYear: {
    type: String
  },
  requestAction: {
    type: String
  },
  requestValue: {
    type: String
  },
  targetTraderTypes: {
    type: Array
  },
  transactionsSessionChainId: {
    type: String
  },
  activeTimeMilliseconds: {
    type: Number
  },
  hooks: {
    type: Array
  },
  periodicCheckIntervalRef: {
    type: String
  }
}, {
  timestamps: true,
  toJSON: {
    virtuals: true,
    transform: (obj, ret) => { delete ret._id }
  },
  usePushEach: true
})

transactionsSessionSchema.methods = {
  getId () {
    return this.id
  },
  getTransactionIds (asObjectIds) {
    let ids = this.transactions || []

    if (asObjectIds) {
      return ids.map(id => mongoose.Types.ObjectId(id))
    }

    return ids
  },
  getChainId () {
    return this.transactionsSessionChainId
  },
  getRequestAction () {
    return this.requestAction
  },
  getRequestMonth () {
    return this.requestMonth
  },
  getRequestYear () {
    return this.requestYear
  },
  getRequestValue () {
    return this.requestValue
  },
  getStatus () {
    return this.overallStatus
  },
  getInitiatorId () {
    return this.initiatorTraderId
  },
  getHooks () {
    return this.hooks || []
  },
  getPeriodicCheckIntervalRef () {
    return this.periodicCheckIntervalRef
  },
  getStartedAt () {
    return this.startedAt
  },
  getExpiresAt () {
    return this.expiresAt
  },
  getTargetTraderTypes () {
    return this.targetTraderTypes
  },
  getActiveTimeMilliseconds () {
    return this.activeTimeMilliseconds
  },

  setStatus (newStatus) {
    this.overallStatus = newStatus
  },
  setPeriodicCheckIntervalRef (ref) {
    this.periodicCheckIntervalRef = ref
  },
  setHooks (hooksArr = []) {
    this.hooks = hooksArr
  },
  setTransactionIds (idsArray) {
    this.transactions = idsArray
  },
  setStartedAt (newDate) {
    this.startedAt = newDate
  },
  setExpiresAt (newDate) {
    this.expiresAt = newDate
  },

  view (full) {
    const view = {
      // simple view
      id: this.id,
      transactions: this.transactions,
      overallStatus: this.overallStatus,
      initiatorTraderId: this.initiatorTraderId,
      initiatorTraderSessionId: this.initiatorTraderSessionId,
      bidVolume: this.bidVolume,
      highestBid: this.highestBid,
      highestBidTransactionId: this.highestBidTransactionId,
      lowestOffer: this.lowestOffer,
      lowestOfferTransactionId: this.lowestOfferTransactionId,
      requestMonth: this.requestMonth,
      requestYear: this.requestYear,
      requestAction: this.requestAction,
      requestValue: this.requestValue,
      askedBid: this.askedBid,
      askedOffer: this.askedOffer,
      targetTraderTypes: this.targetTraderTypes,
      transactionsSessionChainId: this.transactionsSessionChainId,
      activeTimeMilliseconds: this.activeTimeMilliseconds,
      hooks: this.hooks,
      periodicCheckIntervalRef: this.periodicCheckIntervalRef,
      startedAt: this.startedAt,
      expiresAt: this.expiresAt,
      createdAt: this.createdAt,
      updatedAt: this.updatedAt
    }

    return full ? {
      ...view
      // add properties for a full view
    } : view
  }
}

const model = mongoose.model('TransactionsSession', transactionsSessionSchema)

export const schema = model.schema
export default model
