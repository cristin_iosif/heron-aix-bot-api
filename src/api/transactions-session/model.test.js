import { TransactionsSession } from '.'

let transactionsSession

beforeEach(async () => {
  transactionsSession = await TransactionsSession.create({
    transactions: ['test'],
    overallStatus: 'test',
    initiatorTraderId: 'test',
    initiatorTraderSessionId: 'test',
    expiresAt: '2018-03-23T16:33:06.602Z',
    startedAt: '2018-03-23T16:33:06.602Z',
    bidVolume: 1,
    highestBid: 1,
    highestBidTransactionId: 'test',
    lowestOffer: 1,
    lowestOfferTransactionId: 'test',
    requestMonth: 'test',
    requestYear: 'test',
    requestValue: 'test',
    requestAction: 'test',
    askedBid: 1,
    askedOffer: 1,
    targetTraderTypes: 'test',
    transactionsSessionChainId: 'test',
    activeTimeMilliseconds: 1,
    hooks: 'test'
  })
})

describe('view', () => {
  it('returns simple view', () => {
    const view = transactionsSession.view()
    expect(typeof view).toBe('object')
    expect(view.id).toBe(transactionsSession.id)
    expect(view.transactions).toBe(transactionsSession.transactions)
    expect(view.overallStatus).toBe(transactionsSession.overallStatus)
    expect(view.initiatorTraderId).toBe(transactionsSession.initiatorTraderId)
    expect(view.initiatorTraderSessionId).toBe(transactionsSession.initiatorTraderSessionId)
    expect(view.expiresAt).toBe(transactionsSession.expiresAt)
    expect(view.bidVolume).toBe(transactionsSession.bidVolume)
    expect(view.highestBid).toBe(transactionsSession.highestBid)
    expect(view.highestBidTransactionId).toBe(transactionsSession.highestBidTransactionId)
    expect(view.lowestOffer).toBe(transactionsSession.lowestOffer)
    expect(view.lowestOfferTransactionId).toBe(transactionsSession.lowestOfferTransactionId)
    expect(view.requestMonth).toBe(transactionsSession.requestMonth)
    expect(view.requestYear).toBe(transactionsSession.requestYear)
    expect(view.requestAction).toBe(transactionsSession.requestAction)
    expect(view.requestValue).toBe(transactionsSession.requestValue)
    expect(view.askedBid).toBe(transactionsSession.askedBid)
    expect(view.askedOffer).toBe(transactionsSession.askedOffer)
    expect(view.targetTraderTypes).toBe(transactionsSession.targetTraderTypes)
    expect(view.transactionsSessionChainId).toBe(transactionsSession.transactionsSessionChainId)
    expect(view.activeTimeMilliseconds).toBe(transactionsSession.activeTimeMilliseconds)
    expect(view.hooks).toBe(transactionsSession.hooks)
    expect(view.createdAt).toBeTruthy()
    expect(view.updatedAt).toBeTruthy()
  })

  it('returns full view', () => {
    const view = transactionsSession.view(true)
    expect(typeof view).toBe('object')
    expect(view.id).toBe(transactionsSession.id)
    expect(view.transactions).toBe(transactionsSession.transactions)
    expect(view.overallStatus).toBe(transactionsSession.overallStatus)
    expect(view.initiatorTraderId).toBe(transactionsSession.initiatorTraderId)
    expect(view.initiatorTraderSessionId).toBe(transactionsSession.initiatorTraderSessionId)
    expect(view.startedAt).toBe(transactionsSession.startedAt)
    expect(view.expiresAt).toBe(transactionsSession.expiresAt)
    expect(view.bidVolume).toBe(transactionsSession.bidVolume)
    expect(view.highestBid).toBe(transactionsSession.highestBid)
    expect(view.highestBidTransactionId).toBe(transactionsSession.highestBidTransactionId)
    expect(view.lowestOffer).toBe(transactionsSession.lowestOffer)
    expect(view.lowestOfferTransactionId).toBe(transactionsSession.lowestOfferTransactionId)
    expect(view.requestMonth).toBe(transactionsSession.requestMonth)
    expect(view.requestYear).toBe(transactionsSession.requestYear)
    expect(view.requestAction).toBe(transactionsSession.requestAction)
    expect(view.requestValue).toBe(transactionsSession.requestValue)
    expect(view.askedBid).toBe(transactionsSession.askedBid)
    expect(view.askedOffer).toBe(transactionsSession.askedOffer)
    expect(view.targetTraderTypes).toBe(transactionsSession.targetTraderTypes)
    expect(view.transactionsSessionChainId).toBe(transactionsSession.transactionsSessionChainId)
    expect(view.activeTimeMilliseconds).toBe(transactionsSession.activeTimeMilliseconds)
    expect(view.hooks).toBe(transactionsSession.hooks)
    expect(view.createdAt).toBeTruthy()
    expect(view.updatedAt).toBeTruthy()
  })
})
