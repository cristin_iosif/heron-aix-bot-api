import { CompletedTransaction } from '.'

let completedTransaction

beforeEach(async () => {
  completedTransaction = await CompletedTransaction.create({ tradedFinancialInstrumentId: 'test', initiatorTraderId: 'test', marketMakerTraderId: 'test', transactionsSessionId: 'test', transactionId: 'test', dealVolume: 'test', dealPrice: 'test', dealMonth: 'test', dealYear: 'test', dealAction: 'test', dealShards: 'test', closeAction: 'test', dealValue: 'test' })
})

describe('view', () => {
  it('returns simple view', () => {
    const view = completedTransaction.view()
    expect(typeof view).toBe('object')
    expect(view.id).toBe(completedTransaction.id)
    expect(view.tradedFinancialInstrumentId).toBe(completedTransaction.tradedFinancialInstrumentId)
    expect(view.initiatorTraderId).toBe(completedTransaction.initiatorTraderId)
    expect(view.marketMakerTraderId).toBe(completedTransaction.marketMakerTraderId)
    expect(view.transactionsSessionId).toBe(completedTransaction.transactionsSessionId)
    expect(view.transactionId).toBe(completedTransaction.transactionId)
    expect(view.dealVolume).toBe(completedTransaction.dealVolume)
    expect(view.dealPrice).toBe(completedTransaction.dealPrice)
    expect(view.dealMonth).toBe(completedTransaction.dealMonth)
    expect(view.dealYear).toBe(completedTransaction.dealYear)
    expect(view.dealAction).toBe(completedTransaction.dealAction)
    expect(view.dealShards).toBe(completedTransaction.dealShards)
    expect(view.dealValue).toBe(completedTransaction.dealValue)
    expect(view.closeAction).toBe(completedTransaction.closeAction)
    expect(view.createdAt).toBeTruthy()
    expect(view.updatedAt).toBeTruthy()
  })

  it('returns full view', () => {
    const view = completedTransaction.view(true)
    expect(typeof view).toBe('object')
    expect(view.id).toBe(completedTransaction.id)
    expect(view.tradedFinancialInstrumentId).toBe(completedTransaction.tradedFinancialInstrumentId)
    expect(view.initiatorTraderId).toBe(completedTransaction.initiatorTraderId)
    expect(view.marketMakerTraderId).toBe(completedTransaction.marketMakerTraderId)
    expect(view.transactionsSessionId).toBe(completedTransaction.transactionsSessionId)
    expect(view.transactionId).toBe(completedTransaction.transactionId)
    expect(view.dealVolume).toBe(completedTransaction.dealVolume)
    expect(view.dealPrice).toBe(completedTransaction.dealPrice)
    expect(view.dealMonth).toBe(completedTransaction.dealMonth)
    expect(view.dealYear).toBe(completedTransaction.dealYear)
    expect(view.dealAction).toBe(completedTransaction.dealAction)
    expect(view.dealShards).toBe(completedTransaction.dealShards)
    expect(view.dealValue).toBe(completedTransaction.dealValue)
    expect(view.closeAction).toBe(completedTransaction.closeAction)
    expect(view.createdAt).toBeTruthy()
    expect(view.updatedAt).toBeTruthy()
  })
})
